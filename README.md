# Pyneup

Python script to automate and optimize the creation of a softball team lineup, subject to the rules of the McMaster GSA softball league.

## Running pyneup

Simply download the repo and optionally run **pyneup.py** directly at the command line like

```
python3 pyneup.py
```

You will be queried to enter the number of players for the lineup as well as the player ID for each of the players.

Alternatively, you can start the GUI at the command line like

```
python3 gui.py
```

This will allow you to fill out a player roster with genders and position preferences, save and load rosters, and run **pyneup.py** to find lineups.

### Required packages

Pyneup uses the following Python packages:

- numpy
- pandas
- tqdm
- os
- time
- pickle

The pyneup GUI uses the following Python packages:

- numpy
- os
- pandas
- pickle
- PyQt5
- subprocess
- sys