import pickle
import numpy as np

def save_obj(obj, name ):
    with open('database/'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

##############

positions = {}

positions['Abbey'] = np.array([0,2,7,8,9])
positions['Ben'] = np.array([2,3,4,5])
positions['Nate'] = np.array([1,2,3,5,7])
positions['Ashley'] = np.array([2,8,9])
positions['Jason'] = np.array([1,2,5,7,8,9])
positions['MattR'] = np.array([3,5,6])
positions['Sara'] = np.array([0,2,8,9])
positions['Ian'] = np.array([3,4,5,6])
positions['MikeM'] = np.array([2,3,5,6])
positions['Lili'] = np.array([0,2,7,8,9])
positions['MikeT'] = np.array([1,2,3,5,6])
positions['Hao'] = np.array([0,2,7,8,9])
positions['Carmen'] = np.array([0,2,8,9])
positions['Joey'] = np.array([3,4,5,6])

save_obj(positions, 'positions_db')
