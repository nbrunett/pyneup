import numpy as np
import os
import pandas as pd
import pickle
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QApplication, QCheckBox, QFileDialog, QFrame,
                             QGridLayout, QHBoxLayout, QLabel, QLineEdit,
                             QMessageBox, QPushButton, QRadioButton,
                             QScrollArea, QVBoxLayout, QWidget)
import subprocess
import sys

nRows = 50
posLabels = ['C', '1B', '2B', '3B', 'SS', 'LF', 'CF', 'RF', 'SL', 'SR']

class lineupForm(QWidget):
    """
    Create a lineupForm instance.

    This class is the customized widget that makes up the lineup form GUI.
    """

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        """
        Initialize all interactive widgets and organize them into the form.
        """

        # create grid layout for rows of players
        plyrGridBox = QGridLayout()
        self.plyrGridBox = plyrGridBox

        # create header row
        self.plyrGridBox.addWidget(QLabel('Player name'), 0, 2)
        self.plyrGridBox.addWidget(QLabel('Gender'), 0, 3)
        self.plyrGridBox.addWidget(QLabel('Position preferences'), 0, 4)

        self.nPlayers = 1
        self._createPlayers()

        # create button to add players
        addPlayerBtn = QPushButton('Add player')
        addPlayerBtn.clicked.connect(self.addPlayer)
        self.plyrGridBox.addWidget(addPlayerBtn, 51, 2, 1, 1)

        # add the player grid layout to a container frame
        playerGridFrame = QFrame()
        playerGridFrame.setLayout(plyrGridBox)

        # make the grid of player rows vertically scrollable
        scrollArea = QScrollArea()
        scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        scrollArea.setWidgetResizable(True)
        scrollArea.setWidget(playerGridFrame)
        minWidth = playerGridFrame.sizeHint().width() \
                   + scrollArea.verticalScrollBar().sizeHint().width()
        scrollArea.setMinimumWidth(minWidth)

        # create grid layout for form-wide action buttons
        actBtnGridBox = QGridLayout()
        self.actBtnGridBox = actBtnGridBox

        # create button to clear inputs
        clearInputsBtn = QPushButton('Clear inputs')
        clearInputsBtn.clicked.connect(self.clearInputs)
        actBtnGridBox.addWidget(clearInputsBtn, 0, 1)

        # create button to save inputs
        saveInputsBtn = QPushButton('Save inputs')
        saveInputsBtn.clicked.connect(self.saveInputs)
        actBtnGridBox.addWidget(saveInputsBtn, 1, 0)

        # create button to load inputs
        loadInputsBtn = QPushButton('Load inputs')
        loadInputsBtn.clicked.connect(self.loadInputs)
        actBtnGridBox.addWidget(loadInputsBtn, 1, 1)

        # create button to run the lineup finding
        findLineupBtn = QPushButton('Find lineup')
        findLineupBtn.clicked.connect(self.findLineup)
        actBtnGridBox.addWidget(findLineupBtn, 2, 0, 1, 2)

        # create action button container layout to align buttons right
        hBox = QHBoxLayout()
        hBox.addStretch(1)
        hBox.addLayout(actBtnGridBox)

        # add the action button grid layout to a container frame
        actBtnFrame = QFrame()
        actBtnFrame.setLayout(hBox)

        # create top-level layout that holds player and action button grids
        vBox = QVBoxLayout()
        vBox.addWidget(scrollArea)
        vBox.addWidget(actBtnFrame)

        self.setLayout(vBox)

        self.show()

    def _createPlayers(self):
        """
        Build up rows of widgets for each player.

        This should be run by initUI to actually create the instances of all
        the widgets that make up a player row in the form. 50 rows are made
        to accomodate any roster we may have but all except for the first is
        hidden at first. "Adding" and "removing" players simply shows and hides
        the rows made here.
        """

        for i in range(nRows):
            # create button to remove player row
            removePlyrBtn = QPushButton('X')
            removePlyrBtn.clicked.connect(self.removePlayer)
            if i == 0:
                removePlyrBtn.setEnabled(False)
            self.plyrGridBox.addWidget(removePlyrBtn, i+1, 0)

            # create label for row number
            rowNumberLabel = QLabel(str(i+1))
            self.plyrGridBox.addWidget(rowNumberLabel, i+1, 1)

            # create text box for player names
            plyrNameField = QLineEdit()
            self.plyrGridBox.addWidget(plyrNameField, i+1, 2)

            # create buttons to specify player gender
            genRowLayout = QHBoxLayout()
            mRadBtn = QRadioButton('M')
            fRadBtn = QRadioButton('F')
            mRadBtn.setChecked(True)
            genRowLayout.addWidget(mRadBtn)
            genRowLayout.addWidget(fRadBtn)
            genRowFrame = QFrame()
            genRowFrame.setLayout(genRowLayout)
            self.plyrGridBox.addWidget(genRowFrame, i+1, 3, 1, 1)

            # create buttons to specify player position preferences
            posPrefRowLayout = QHBoxLayout()
            for position in posLabels:
                posPrefRowLayout.addWidget(QCheckBox(position))
            posPrefRowFrame = QFrame()
            posPrefRowFrame.setLayout(posPrefRowLayout)
            self.plyrGridBox.addWidget(posPrefRowFrame, i+1, 4, 1, 1)

            if i != 0:
                removePlyrBtn.hide()
                rowNumberLabel.hide()
                plyrNameField.hide()
                genRowFrame.hide()
                posPrefRowFrame.hide()

    def addPlayer(self):
        """
        Add a player row to the form.

        All player rows are created when the lineupForm object is initialized
        so this simply makes a new row visible.
        """

        self.nPlayers += 1
        firstInd = 8 + 5*(self.nPlayers-2)
        self.plyrGridBox.itemAt(firstInd).widget().show()
        self.plyrGridBox.itemAt(firstInd+1).widget().show()
        self.plyrGridBox.itemAt(firstInd+2).widget().show()
        self.plyrGridBox.itemAt(firstInd+3).widget().show()
        self.plyrGridBox.itemAt(firstInd+4).widget().show()

        # enable 1st remove player button if adding 2nd player now
        if self.nPlayers == 2:
            rmPlyrBtn = self.plyrGridBox.itemAt(3).widget()
            rmPlyrBtn.setEnabled(True)

        self.repaint()

    def _replaceRowWithNext(self, rowInd):
        """
        Replace the contents of the given row with those from the one below.

        To get the effect of removing a row, the contents of the row below are
        placed into the row being removed which effectively shifts the contents
        up and overwrites the row being deleted. To keep things tidy, this also
        sets the row below to the defaults so it is effectively empty.
        """

        # grab objects making up the row to delete and the one below
        rmPlyrBtn = self.plyrGridBox.itemAt(rowInd).widget()
        nxtRmPlyrBtn = self.plyrGridBox.itemAt(rowInd+5).widget()
        rowNumberLabel = self.plyrGridBox.itemAt(rowInd+1).widget()
        nxtRowNumberLabel = self.plyrGridBox.itemAt(rowInd+6).widget()
        plyrName = self.plyrGridBox.itemAt(rowInd+2).widget()
        nxtPlyrName = self.plyrGridBox.itemAt(rowInd+7).widget()
        genderFrame = self.plyrGridBox.itemAt(rowInd+3).widget()
        nxtGenderFrame = self.plyrGridBox.itemAt(rowInd+8).widget()
        plyrPrfsFrame = self.plyrGridBox.itemAt(rowInd+4).widget()
        nxtPlyrPrfsFrame = self.plyrGridBox.itemAt(rowInd+9).widget()

        # copy contents of row below into row being deleted
        plyrName.setText(nxtPlyrName.text())
        genderBtns = genderFrame.findChildren(QRadioButton)
        nxtGenderBtns = nxtGenderFrame.findChildren(QRadioButton)
        genderBtns[0].setChecked(nxtGenderBtns[0].isChecked())
        genderBtns[1].setChecked(nxtGenderBtns[1].isChecked())
        plyrPrfsBtns = plyrPrfsFrame.findChildren(QCheckBox)
        nxtPlyrPrfsBtns = nxtPlyrPrfsFrame.findChildren(QCheckBox)
        for i in range(len(plyrPrfsBtns)):
            currBtn = plyrPrfsBtns[i]
            nextBtn = nxtPlyrPrfsBtns[i]
            currBtn.setChecked(nextBtn.isChecked())

        # set row below to defaults
        nxtPlyrName.setText('')
        nxtGenderBtns[0].setChecked(True)
        nxtGenderBtns[1].setChecked(False)
        for i in range(len(nxtPlyrPrfsBtns)):
            nxtPlyrPrfsBtns[i].setChecked(False)

    def _removePlayer(self, rmPlyrBtn):
        """
        Remove the player row associated with the given remove button.

        This handles running _replaceRowWithNext from the correct row and the
        appropriate number of times to remove the specified player row. This
        also handles hiding the empty row left after doing the shifting. It is
        separate here from the removePlayer method so that the row can be
        identified by the button pressed in removePlayer but this method can
        also be used internally by the object.
        """

        # grab objects making up the player row
        rmPlyrBtnInd = self.plyrGridBox.indexOf(rmPlyrBtn)
        rowNumberLabel = self.plyrGridBox.itemAt(rmPlyrBtnInd+1).widget()
        plyrName = self.plyrGridBox.itemAt(rmPlyrBtnInd+2).widget()
        genderFrame = self.plyrGridBox.itemAt(rmPlyrBtnInd+3).widget()
        plyrPrfsFrame = self.plyrGridBox.itemAt(rmPlyrBtnInd+4).widget()

        # shift rows up starting below the row being removed
        for i in range(nRows-self.nPlayers):
            rowInd = rmPlyrBtnInd + 5*i
            self._replaceRowWithNext(rowInd)

        # hide the empty player row
        rowInd = 5*self.nPlayers - 2
        rmPlyrBtn = self.plyrGridBox.itemAt(rowInd).widget()
        rowNumberLabel = self.plyrGridBox.itemAt(rowInd+1).widget()
        plyrName = self.plyrGridBox.itemAt(rowInd+2).widget()
        genderFrame = self.plyrGridBox.itemAt(rowInd+3).widget()
        plyrPrfsFrame = self.plyrGridBox.itemAt(rowInd+4).widget()
        rmPlyrBtn.hide()
        rowNumberLabel.hide()
        plyrName.hide()
        genderFrame.hide()
        plyrPrfsFrame.hide()

        self.nPlayers -= 1

        # disable remove player button if only one left
        if self.nPlayers == 1:
            rmPlyrBtn = self.plyrGridBox.itemAt(3).widget()
            rmPlyrBtn.setEnabled(False)

    def removePlayer(self):
        """
        Remove the player row associated with the button clicked.

        This is here for catching which button was pressed so the row to be
        removed can be determined. The actual player removal is handled by
        _removePlayer.
        """

        rmPlyrBtn = self.sender()
        self._removePlayer(rmPlyrBtn)

    def _getInputs(self):
        """
        Return inputs of all visible input widgets.
        """

        allInputs = list()
        for i in range(self.nPlayers):
            rowInputs = list()
            plyrName = self.plyrGridBox.itemAt((i*5)+5).widget()
            genderFrame = self.plyrGridBox.itemAt((i*5)+6).widget()
            genderBtns = genderFrame.findChildren(QRadioButton)
            plyrPrfsFrame = self.plyrGridBox.itemAt((i*5)+7).widget()
            plyrPrfsBtns = plyrPrfsFrame.findChildren(QCheckBox)

            rowInputs.append(plyrName.text())
            for j in range(len(genderBtns)):
                rowInputs.append(genderBtns[j].isChecked())
            for j in range(len(plyrPrfsBtns)):
                rowInputs.append(plyrPrfsBtns[j].isChecked())
            allInputs.append(rowInputs)

        return allInputs

    def clearInputs(self):
        """
        Remove all player rows and leave the 1st row empty.
        """

        while self.nPlayers > 0:
            rowInd = 5*self.nPlayers - 2
            rmPlyrBtn = self.plyrGridBox.itemAt(rowInd).widget()
            self._removePlayer(rmPlyrBtn)
        self.addPlayer()

    def saveInputs(self):
        """
        Prompt for a save file name and write the form contents as a CSV there.
        """

        name = QFileDialog.getSaveFileName(self)
        if name[0] != '':
            inputs = self._getInputs()
            inputs = pd.DataFrame(inputs)
            inputs.to_csv(name[0], index=False,
                          header=['Player name', 'Male', 'Female']+posLabels)

    def loadInputs(self):
        """
        Prompt for a load file name and fill form with loaded inputs.
        """

        name = QFileDialog.getOpenFileName(self)
        if name[0] != '':
            try:
                inputs = pd.read_csv(name[0], sep=',', header=0,
                                     dtype={'Player name': str,
                                            'Male': bool, 'Female': bool,
                                            'C': bool, '1B': bool, '2B': bool,
                                            '3B': bool, 'SS': bool, 'LF': bool,
                                            'CF': bool, 'RF': bool, 'SL': bool,
                                            'SR': bool})
            # gracefully handle read_csv failing
            except:
                msg = 'Could not load a roster from the selected file.'
                QMessageBox.about(self, 'Error', msg)
            # read_csv did not throw and exceptions
            else:
                # no missing data
                if not inputs.isnull().any().any():
                    self.clearInputs()
                    for i in range(inputs.shape[0]):
                        ind = (i*5)+5
                        plyrName = self.plyrGridBox.itemAt(ind).widget()
                        genderFrame = self.plyrGridBox.itemAt(ind+1).widget()
                        genderBtns = genderFrame.findChildren(QRadioButton)
                        plyrPrfsFrame = self.plyrGridBox.itemAt(ind+2).widget()
                        plyrPrfsBtns = plyrPrfsFrame.findChildren(QCheckBox)

                        plyrName.setText(inputs['Player name'][i])
                        genderBtns[0].setChecked(inputs['Male'][i])
                        genderBtns[1].setChecked(inputs['Female'][i])
                        for j,position in enumerate(posLabels):
                            plyrPrfsBtns[j].setChecked(inputs[position][i])

                        if i > 0:
                            self.addPlayer()
                # handle missing data
                else:
                    msg = 'Could not load a roster from the selected file.'
                    QMessageBox.about(self, 'Error', msg)

    def findLineup(self):
        """
        Run the lineup finder with the lineup form inputs.
        """
        repoDir = os.path.realpath(__file__)
        repoDir = os.path.dirname(repoDir) + '/'

        pyneupInputs = str(self.nPlayers) + '\n'

        guiInputs = self._getInputs()

        prefDict = dict()
        for i in range(len(guiInputs)):
            name = guiInputs[i][0]
            prefDict[name] = np.array(guiInputs[i][3:], dtype=int)
            prefDict[name] *= np.arange(1, 11)
            prefDict[name] -= 1
            inds = np.where(prefDict[name]>=0)
            prefDict[name] = prefDict[name][inds]
            pyneupInputs += name + '\n' #replace line above after !1 merge
        with open(repoDir+'database/positions_db.pkl', 'wb') as f:
            pickle.dump(prefDict, f, pickle.HIGHEST_PROTOCOL)

        subprocess.run([sys.executable, repoDir+'pyneup.py'],
                       input=pyneupInputs.encode('utf-8'))

        os.remove(repoDir+'database/positions_db.pkl')

    def closeEvent(self, event):
        """
        Ask if ready to quit lineup form.
        """

        msg = 'Are you sure you want to quit?'
        ans = QMessageBox.question(self, 'Last Chance', msg,
                                       QMessageBox.Yes | QMessageBox.No,
                                       QMessageBox.No)

        if ans == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()


if __name__ == '__main__':
    app = QApplication([])
    form = lineupForm()
    sys.exit(app.exec_())
