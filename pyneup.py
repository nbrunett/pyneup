import numpy as np
import pandas as pd
from tqdm import tqdm, trange
import os
import time
import pickle

###########

def load_obj(name):
    with open('database/' + name + '.pkl', 'rb') as f:
        return pickle.load(f)

def make_frames(player_ids, player_mf, inning_list, position_names, fill_order, positions):
    '''Function to generate random lineup dataframe, assigning a player to each
       position for each inning.
    
       Input: array of player ids, array of player genders, array of inning names,
              array of position names

       Output: dataframe assigning a player id to each position for each inning,
               dataframe of genders for each position assignment
    '''
    # Initiate dataframes to fill
    frame_ids = pd.DataFrame(-1, index=position_names, columns=inning_list)
    frame_mf = pd.DataFrame(-1, index=position_names, columns=inning_list)

    # Loop through innings and positions
    for i in range(inning_list.size):
        players_on_field = []
        for j in fill_order: # Fill positions in specified order
            players_for_this_position = []
            for play in players:
                ind1 = np.where(positions['{}'.format(play)] == j)[0]
                if ind1.size > 0: # Find players eligible for current position
                    players_for_this_position.append(play)

            mask_on_field = np.isin(np.asarray(players_for_this_position), np.asarray(players_on_field)) # Make sure players aren't already on field
            rand_player = np.random.choice(np.asarray(players_for_this_position)[~mask_on_field], size=1, replace=False)[0] # Select player randomly
            frame_ids.iloc[j,i] = rand_player # Add to dataframe
            ind_mf = np.where(players == np.array([rand_player]))[0]
            frame_mf.iloc[j,i] = player_mf[ind_mf] # Add to dataframe
            players_on_field.append(rand_player)

    return frame_ids, frame_mf
    

def likelihood(frame_ids, frame_mf, player_ids, inning_list, positions):
    '''Function to evaluate the 'likelihood' of the randomly generated
       lineup being optimal.  The lineup that will be selected is the
       one which gives the highest 'likelihood'.  Likelihood is determined
       by the following factors:

       - Are three females on field at all times?
       - Are all players playing roughly an equal number of innings?
       - Are players playing equal innings in infield and outfield?
    '''
    # Define factors used as penalties
    inning_cts_factor = 10.
    outfield_infield_factor = 1.

    mf_sum = np.array(frame_mf.sum(axis=0)) # Count females in each inning

    # Initiate arrays for later
    player_cts = np.zeros(len(players))
    infield_cts = np.zeros(len(players))
    outfield_cts = np.zeros(len(players))

    like = 0 # Initiate likelihood

    # Number of females per inning
    
    if mf_sum[mf_sum < 3].size > 0: # Ensure at least three females
        return -np.inf, player_cts, infield_cts

    # Balance total innings played per player
    # Balance infield and outfield

    infield = frame_ids.iloc[0:5,:]
    outfield = frame_ids.iloc[5:,:]

    for i in range(len(players)):
        player_cts[i] = np.where(frame_ids == players[i])[0].size
        infield_cts[i] = np.where(infield == players[i])[0].size
        outfield_cts[i] = np.where(outfield == players[i])[0].size

    cts_var = np.var(player_cts)
    like += -1 * inning_cts_factor * np.exp(cts_var) # Likelihood penalty

    out_in_diff = np.abs(outfield_cts - infield_cts)
    like += -1 * np.sum(out_in_diff) * outfield_infield_factor # Likelihood penalty
    

    # Space bench innings evenly

    return like, player_cts, out_in_diff

#############
'''
0 -> C 5
1 -> 1B 3
2 -> 2B 11
3 -> 3B 7
4 -> SS 3
5 -> LF 8
6 -> CF 5
7 -> RF 5
8 -> SL 7
9 -> SR 7
'''

fill_order = np.array([4,1,0,6,7,3,8,9,5,2])

#############

no_of_players = int(input("How many players? "))

#players = ['Abbey','Ben','Nate','Ashley','Jason','MattR','Sara','Ian','MikeM','Lili','MikeT','Hao','Carmen','Joey']
players = []

print("")
for i in np.arange(no_of_players):
    players.append(input("Player {} name? ".format(i+1)))
print("")

positions = load_obj('positions_db')

player_mf = np.array([1,0,0,1,0,0,1,0,0,1,0,0,1,0])
inning_list = np.array(['inn1','inn2','inn3','inn4','inn5','inn6','inn7','inn8','inn9'])
position_names = np.array(['C','1B','2B','3B','SS','LF','CF','RF','SL','SR'])

Nit = 1000

like = -np.inf

for it in trange(Nit, desc='Iterating to find optimal lineup'):
    frame_ids, frame_mf = make_frames(players, player_mf, inning_list, position_names, fill_order, positions)
    like_new, counts, out_in = likelihood(frame_ids, frame_mf, players, inning_list, positions)

    if like_new > like:
        final_lineup = frame_ids
        final_mf = frame_mf
        final_counts = counts
        final_out_in = out_in
        like = like_new

time.sleep(2)

print("")
print("### Lineup ###")
print("")
print(final_lineup)

time.sleep(2)

print("")
print("### Constraint results ###")
print("")
print(pd.DataFrame({'No. of innings': final_counts, 'Outfield/infield differential': final_out_in}, index=players))
print("")

time.sleep(2)

print("")
print("Likelihood: ", like)
print("")

time.sleep(2)

print("### Creating latex file ###")
print("")

with open('lineup_table.tex','w') as tf:
    tf.write(final_lineup.to_latex())

os.system("pdflatex lineup.tex")

